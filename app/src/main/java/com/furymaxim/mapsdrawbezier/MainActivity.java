package com.furymaxim.mapsdrawbezier;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.operation.IsSimpleOp;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    FrameLayout fragmentOverlay;
    Boolean isMapMoveable = false; // to detect map is movable
    Button btnDrawState, btnClean;
    private GoogleMap googleMap;
    private boolean screenLeave = false;
    int source = 0;
    int destination = 1;
    double latitude;
    double longitude;
    private ArrayList<LatLng> val = new ArrayList<>();

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentOverlay = findViewById(R.id.fram_map);
        btnDrawState = findViewById(R.id.btn_draw_State);
        btnClean = findViewById(R.id.btn_clean);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        fragmentOverlay.setOnTouchListener(new View.OnTouchListener() {
            @Override

            public boolean onTouch(View v, MotionEvent event) {

                if (isMapMoveable) {
                    float x = event.getX();
                    float y = event.getY();

                    int xCoords = Math.round(x);
                    int yCoords = Math.round(y);

                    Point pointCoord = new Point(xCoords, yCoords);

                    LatLng latLng = googleMap.getProjection().fromScreenLocation(pointCoord);
                    latitude = latLng.latitude;
                    longitude = latLng.longitude;

                    LatLng point = new LatLng(latitude, longitude);

                    int eventAction = event.getAction();

                    switch (eventAction) {
                        case MotionEvent.ACTION_DOWN:
                            screenLeave = false;

                        case MotionEvent.ACTION_MOVE:
                            val.add(new LatLng(latitude, longitude));
                            screenLeave = false;
                            drawProgress();

                        case MotionEvent.ACTION_UP:
                            if (!screenLeave) {
                                screenLeave = true;
                            } else {
                                isMapMoveable = false;
                                source = 0;
                                destination = 1;
                                btnDrawState.setText("CLICK TO DRAW");
                                drawPolygon();

                            }
                            break;
                    }

                    if (isMapMoveable) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            }
        });

        btnDrawState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                val.clear();
                googleMap.clear();
                isMapMoveable = !isMapMoveable;
                if(isMapMoveable == true){
                    btnDrawState.setText("YOU CAN DRAW NOW");
                }else{
                    btnDrawState.setText("CLICK TO DRAW");
                }
            }
        });

        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                val.clear();
            }
        });
    }


    public void drawProgress() {

        if (val.size() > 1) {
            googleMap.addPolyline(new PolylineOptions().add(val.get(source), val.get(destination)).width(8).color(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));
            source++;
            destination++;
        }

    }


    private void  drawPolygon() {



        val.add(val.get(0));

        if(isIntersects(googleMapCoordsToJTS(val))){
            btnDrawState.setEnabled(false);
            Toast.makeText(getApplicationContext(),"Некорректная фигура. Обнаружено пересечение!",Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    googleMap.clear();
                    val.clear();
                    btnDrawState.setEnabled(true);
                }
            }, 500);
        }else {

            PolygonOptions polygonOptions = new PolygonOptions();
            polygonOptions.addAll(val);
            polygonOptions.strokeColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
            polygonOptions.strokeWidth(8);
            polygonOptions.fillColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            googleMap.addPolygon(polygonOptions);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        LatLng moscow = new LatLng(55.75, 37.62);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(moscow));
        googleMap.setMinZoomPreference(9);

    }


    private Coordinate[] googleMapCoordsToJTS(ArrayList<LatLng> values){
        Coordinate[] coordinates = new Coordinate[values.size()];

        for(int i = 0; i < values.size();i++) {
            coordinates[i] = new Coordinate(val.get(i).latitude, val.get(i).longitude);
        }

        return coordinates;
    }

    private boolean isIntersects(Coordinate[] coordinates){


        GeometryFactory geometryFactory = new GeometryFactory();
        LinearRing shell = geometryFactory.createLinearRing(coordinates);
        Geometry jtsPolygon = geometryFactory.createPolygon(shell);

        IsSimpleOp validator = new IsSimpleOp(jtsPolygon);
        return  !validator.isSimple();
    }
}
